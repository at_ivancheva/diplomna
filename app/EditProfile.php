<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EditProfile extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'gender', 'email', 
    ];
}
