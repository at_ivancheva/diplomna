<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    protected $fillable = [
        'name', 'description', 'avatar', 'avatar_name', 'team_sport', 'players'
    ];
}
