<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Sport;
use Auth;
use Illuminate\Support\Facades\Input;

class SportController extends Controller
{
	//View all sports function
    public function view() {
    	$sports_entries = Sport::all();

        return view('sports.all_sports', compact('sports_entries'));
	}

	//Add new sport
    public function index() {
        return view('sports.sports');
    }
	
	public function add() {
        $sports_entry = new Sport();

        $sports_entry->name = Input::get('name');
        $sports_entry->description = Input::get('description');
        $sports_entry->team_sport = Input::get('team_sport');
        $sports_entry->players = Input::get('players');

        $sports_entry->save();

        return redirect('sports');
    }
}
