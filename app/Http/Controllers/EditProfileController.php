<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use App\User;
use Auth;

class EditProfileController extends Controller
{
	public function index()
    {
        $entries = User::where('id', '=', Auth::id())->get();
    	return view('auth/editprofile', compact('entries'));
    }

    public function update() {
        $entry = User::find(Auth::id());
        $entry->first_name = Input::get('first_name');
        $entry->last_name = Input::get('last_name');
        $entry->gender = Input::get('gender');
        $entry->email = Input::get('email');
        $entry->save();

        return redirect('editprofile');
    }

    public function add() {

        $this->middleware('auth');
        $file = Request::file('filefield');

        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
        $entry = User::find(Auth::id());
        $entry->mime = $file->getClientMimeType();
        $entry->avatar_name = $file->getFilename().'.'.$extension;

        $entry->save();

        return redirect('editprofile');   
    }

    public function get($avatar_name){

        $entry = User::where('avatar_name', '=', $avatar_name)->firstOrFail();
        $file = Storage::disk('local')->get($entry->avatar_name);

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);
    }
    
}
