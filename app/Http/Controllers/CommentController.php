<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Comment;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::all();

        return view('attachment.index');
    }

	 public function add()
    {
   		$this->middleware('auth');
        $comment = new Comment();

        $comment->user_id = Auth::id();
        $comment->content = Input::get('content');

        //$post->user()->associate($user);
       // $post->save();

        $comment->save();

        return redirect('attachment');
    }
}
