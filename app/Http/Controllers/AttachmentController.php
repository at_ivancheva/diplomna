<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Attachment;
use App\Comment;
use Request;
use DB;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;


class AttachmentController extends Controller
{
    public function index()
    {
        $entries = Attachment::all();
        $videos = DB::table('attachments')->where('mime', 'video/mp4')->first();
        $comments = Comment::all();

        return view('attachments.index', compact('entries', 'videos', 'comments'));
    }

    public function add() {
        $this->middleware('auth');
        $file = Request::file('filefield');

        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
        $entry = new Attachment();
        $entry->user_id = Auth::id();
        $entry->mime = $file->getClientMimeType();
        $entry->description = Input::get('description');
        $entry->filename = $file->getFilename().'.'.$extension;

        $entry->save();

        return redirect('attachment');

    }
    public function get($filename){

        $entry = Attachment::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($entry->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);
    }

    public function getvideo($filename){
       $entry = Attachment::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($entry->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);
    }
}
