<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');

    	//Attachments upload
    Route::get('attachment', 'AttachmentController@index');
    Route::get('attachment/get/{filename}', [
        'as' => 'getattachment', 'uses' => 'AttachmentController@get']);
    Route::get('attachment/getvideo/{filename}', [
        'as' => 'getvideo', 'uses' => 'AttachmentController@getvideo']);
    Route::post('attachment/add',[
    'as' => 'addattachment', 'uses' => 'AttachmentController@add']);

    //Sports upload
    Route::get('/sports', 'SportController@index');         //add new sport
    Route::get('/all_sports', 'SportController@view');      //view all sports
    Route::get('/sport/get/{name}', [
        'as' => 'getsport', 'uses' => 'SportController@get']);
    Route::post('sports/add',[
    'as' => 'addsports_enrty', 'uses' => 'SportController@add']);

    //Comments
    Route::post('comment/add',[
    'as' => 'addComment', 'uses' => 'CommentController@add']);

    //Edit Profile
    Route::get('/editprofile', 'EditProfileController@index');
    Route::post('edit_profile/update',[
    'as' => 'addentry', 'uses' => 'EditProfileController@update']);
    Route::get('/editProfile/get/{profile_picture}', [
        'as' => 'getProfilePicture', 'uses' => 'EditProfileController@get']);       //display profile pic
    Route::post('editProfile/add',[
    'as' => 'addProfilePicture', 'uses' => 'EditProfileController@add']);           //add profile pic
});
Route::resource('comment', 'CommentController');

