@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    @foreach($entries as $entry)
                        <img src="{{route('getProfilePicture', $entry->avatar_name)}}" alt="ALT NAME" class="img-responsive" style="width:50px;height:50px;border-radius: 20px;" />
                    @endforeach
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit_profile/update') }}">
                        {!! csrf_field() !!}

                        
                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            @foreach($entries as $entry)
                                <label class="col-md-4 control-label">First Name</label>
                                <input type="text" class="form-control" name="first_name" value="{{$entry->first_name}}" >

                                <label class="col-md-4 control-label">Last Name</label>
                                <input type="text" class="form-control" name="last_name" value="{{$entry->last_name}}" >

                                <label class="col-md-4 control-label">Gender</label>
                                <label>
                                    <input type="radio" class="form-control" name="gender"  value="male"> Male
                                    <input type="radio" class="form-control" name="gender"  value="female"> Female
                                    <input type="radio" class="form-control" name="gender"  value="other"> Other
                                </label>

                                <label class="col-md-4 control-label">E-mail</label>
                                <input type="text" class="form-control" name="email" value="{{$entry->email}}" >
                            @endforeach
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Update
                                </button>
                            </div>
                        </div>
                    </form>
                    <form action="{{route('addProfilePicture', [])}}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="file" name="filefield">
                        <input type="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection