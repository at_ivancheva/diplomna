@extends('layouts.app')
@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Add new Sport</div>

                    <div class="panel-body">
                        <form action="{{route('addsports_enrty', [])}}" method="post" enctype="multipart/form-data">
                             {!! csrf_field() !!}

                            <label class="col-md-4 control-label">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                            <label class="col-md-4 control-label">Brief description of this sport</label>
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}">

                            <label class="col-md-4 control-label">Team sport or Individual</label>
                                <label>
                                    <input type="radio" class="form-control" name="team_sport"  value="Team Sport"> Team Sport
                                    <input type="radio" class="form-control" name="team_sport"  value="Individual Sport"> Individual
                                </label>
                            </br>
                            <label class="col-md-4 control-label">Number of players</label>
                            <input type="text" class="form-control" name="players" value="{{ old('playesr') }}">

                            <input type="submit">
                        </form>

                       

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
