@extends('layouts.app')
@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @foreach($sports_entries as $sport_entry)

                                            
                                            <div class="caption">
                                                <p>{{$sport_entry->name}}</p>
                                                <p>{{$sport_entry->description}}</p>
                                                <p>{{$sport_entry->team_sport}}</p>
                                                <p>{{$sport_entry->players}}</p>
                                                <hr>
                                            </div>

                                @endforeach  

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
