@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">File Upload</div>

                    <div class="panel-body">
                        <form action="{{route('addattachment', [])}}" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <input type="file" name="filefield">
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}" placeholder="Add description...">
                            <input type="submit">
                        </form>

                        <h1> Petur Petrov's list</h1>
                        <div class="row">
                            <ul class="thumbnails">
                               @foreach($entries as $entry)

                                        <img src="{{route('getattachment', $entry->filename)}}" alt="ALT NAME" class="img-responsive" style="width:600px;height:330px;"/>
                                        <div class="caption">
                                            <p>{{$entry->description}}</p>
                                        </div>
                                @endforeach
                                @foreach($comments as $comment)
                                <form action="{{route('addComment', [])}}" method="post" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            <label>Comment</label></br>
                                            <input type="text" class="form-control" name="content" value="{{ old('content') }}">
                                            <input type="submit">
                                        </form>
                                            <div class="caption">
                                                <p>{{$comment->content}}</p>
                                            </div>
                                        @endforeach
                                @foreach($videos as $video)
                                    <video height="300px" controls>
                                                <source src="{{route('getvideo', $entry->filename)}}" type="video/mp4">
                                                <source src="{{route('getvideo', $entry->filename)}}" type="video/ogg">
                                                Your browser does not support the video tag.
                                            </video>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
